import React from 'react'
import { Media } from 'reactstrap';
import { Card, CardImg, CardImgOverlay, CardText, CardBody,
    CardTitle } from 'reactstrap';
import App from '../App';

function Menu(){     
           const menu = dishes.map((dish) => {
            return (
              <div  className="col-12 col-md-5 m-1">
                <Card key={dish.id}
                  onClick={() => this.onDishSelect(dish)}>
                  <CardImg width="100%" src={dish.image} alt={dish.name} />
                  <CardImgOverlay>
                      <CardTitle>{dish.name}</CardTitle>
                  </CardImgOverlay>
                </Card>
              </div>
            );
        });
    
    return (
        <div className="container">
        <div className="row">
          <Media list>
              {menu}
          </Media>
        </div>
      </div>
    )
}

onDishSelect(dish){
selectedDish: dish
}


export default Menu